# game-of-life

To install dependencies:

```bash
bun install
```

To Build:

```bash
bun run build
```

To test:

```bash
bun test
```

You can now direct your browser to
`file:///<PATH-TO-REPO>/game-of-life/demo/index.html`

This project was created using `bun init` in bun v1.0.3.
[Bun](https://bun.sh) is a fast all-in-one JavaScript
runtime.
