
/// <reference lib="dom" />
/// <reference lib="dom.iterable" />
/* the above lines are necessary for the LSP to accept browser stuff */
/* perhaps a conflict with bun types?                                */

/*
another zoom could allow selection of quadrant to zoom in
movement keys are choppy, should use up/down
perhaps a key show the center of mass, the place with the most cells?

neighbourhoodSize | outcome
...               | death
3                 | life
4                 | same
...               | death

file:///home/gregory/Desktop/code/dojo/game-of-life/demo/index.html
https://gitlab.com/grenle-small-projects/game-of-life
https://gitlab.com/grenle-small-projects/game-of-life.git

 */

import {
    Viewport,
} from './types/types.ts'

import { Board } from './board/board.ts'

import { getCanvasInfo } from './canvas-info/canvas-info.ts'

import { stepper } from './display/display.ts'

import generateCells from './generate-cells/generate-cells.ts'

function getSpeedDisplay(){
    let speed: HTMLParagraphElement | null = null
    speed = document.querySelector("#speed")
    let displaySpeed = (_: number) => {}
    if(speed !== null){
        displaySpeed = (t: number) =>
        speed ? speed.innerText = t.toFixed(0) : (_: number) => {}
    }
    return displaySpeed
}

function init(){
    const viewport = {
        viewX: 0,
        viewY: 0,
        cellSize: 10,
    }
    globalThis.addEventListener('keydown', registerKeys(viewport))
    const displaySpeed = getSpeedDisplay()
    const canvasInfo = getCanvasInfo()
    if(canvasInfo !== null){
        const board = Board(generateCells.random(800, 800))
        const { context } = canvasInfo
        const coca = context.canvas
        coca.width = 800
        if(coca === null){
            console.log('context/canvas null')
        }
        else {
            stepper(viewport, context, board, displaySpeed)
        }
    }
}

function registerKeys(viewport: Viewport){
    return function keyboardAction(keyEvent: KeyboardEvent){
        switch(keyEvent.code){
            case 'ArrowLeft':
                viewport.viewX = viewport.viewX - 10
                break
            case 'ArrowRight':
                viewport.viewX = viewport.viewX + 10
                break
            case 'ArrowUp':
                viewport.viewY = viewport.viewY - 10
                break
            case 'ArrowDown':
                viewport.viewY = viewport.viewY + 10
                break
            case 'KeyI':
                viewport.cellSize = Math.max(viewport.cellSize - 2, 1)
                break
            case 'KeyO':
                viewport.cellSize = Math.min(viewport.cellSize + 2, 10) 
                break
        }
    }
}

globalThis.addEventListener('load', init)

