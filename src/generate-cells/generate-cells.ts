import { Cells } from '../types/types.ts'

function random(width: number, height: number){
    const cells: Cells = []
    for(let x = 0; x < width; x++){
        for(let y = 0; y < height; y++){
            if(Math.random() > 0.5){
                cells.push([x, y])
            }
        }
    }
    return cells
}

export default { random }
