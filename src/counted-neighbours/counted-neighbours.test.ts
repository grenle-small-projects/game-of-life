import { describe, expect, test } from 'bun:test'

import {
    _matchMatrix,
    CountedNeighbours,
    MatchMatrixProcs,
} from './counted-neighbours.ts'

import {Cell} from '../types/types.ts'

describe('matrixMatch', () => {

    const simpleMatch: MatchMatrixProcs<string> = {
        noX:  _ => 'noX',
        noY:  _ => 'noY',
        live: _ => 'live',
    }

    const simpleBoard: Array<Array<number>> = [
        [],
        [ 0 ],
    ]

    test('_matchCellType empty board noX', () => {
        const match = _matchMatrix([])
        const res = match(simpleMatch)([0, 0])
        expect(res).toStrictEqual('noX')
    })

    test('_matchCellType simple board noX', () => {
        const match = _matchMatrix(simpleBoard)
        const res = match(simpleMatch)([2, 0])
        expect(res).toStrictEqual('noX')
    })

    test('_matchCellType simple board noY', () => {
            const match = _matchMatrix(simpleBoard)
        const res = match(simpleMatch)([1, 1])
        expect(res).toStrictEqual('noY')
    })

    
    test('_matchCellType simple board live', () => {
        const match = _matchMatrix(simpleBoard)
        const res = match(simpleMatch)([1, 0])
        expect(res).toStrictEqual('live')
    })

})


describe('CountedNeighbours', () => {

    test('empty CountedNeighbours yields nothing', () => {
        const counted = CountedNeighbours()
        const countedArray = Array.from(counted.yieldCounts())
        expect(countedArray).toBeEmpty()
    })

    test('add 1 cell to empty CountedNeighbours, yield [ ...cell, 1]', () => {
        const counted = CountedNeighbours()
        const neighbours: Cell[] = [ [ 2, 8 ] ]
        counted.accumulateNeighbours(neighbours)
        const countedArray = Array.from(counted.yieldCounts())
        expect(countedArray).toStrictEqual([ [ 2, 8, 1 ] ])
    })

    test('add 1 cell twice to empty CountedNeighbours, yield [ ...cell, 2]', () => {
        const counted = CountedNeighbours()
        const neighbours: Cell[] = [ [ 2, 8 ] ]
        counted.accumulateNeighbours(neighbours)
        counted.accumulateNeighbours(neighbours)
        const countedArray = Array.from(counted.yieldCounts())
        expect(countedArray).toStrictEqual([ [ 2, 8, 2 ] ])
    })

    test('add 2 different cells to empty, correct yield', () => {
        const counted = CountedNeighbours()
        const neighbours: Cell[] = [ [ 2, 8 ], [ 1, 7] ]
        counted.accumulateNeighbours(neighbours)
        const countedArray = Array.from(counted.yieldCounts())
        expect(countedArray).toStrictEqual([ [ 1, 7, 1 ], [ 2, 8, 1 ] ])
    })


    test('add 2 different cells sharing x to empty, correct yield', () => {
        const counted = CountedNeighbours()
        const neighbours: Cell[] = [ [ 2, 8 ], [ 2, 7] ]
        counted.accumulateNeighbours(neighbours)
        const countedArray = Array.from(counted.yieldCounts())
        expect(countedArray).toStrictEqual([ [ 2, 7, 1 ], [ 2, 8, 1 ] ])
    })

})
