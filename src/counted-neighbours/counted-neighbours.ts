import { Cell, CellAndCount } from '../types/types.ts'

export type MatchMatrixProcs<Codomain> = {
    noX: (cell: Cell) => Codomain,
    noY: (cell: Cell) => Codomain,
    live: (cell: Cell) => Codomain,
}

// not really board, more matrix?
export function _matchMatrix(matrix: Array<Array<number>>){
    return function<Codomain>(procs: MatchMatrixProcs<Codomain>){
       return function([x, y]: Cell){
           if(matrix[x] === undefined){
               return procs.noX([x, y])
           }
           if(matrix[x][y] === undefined){
               return procs.noY([x, y])
           }
           return procs.live([x, y])
       } 
    }
}

export function CountedNeighbours(){
    const scores: Array<Array<number>> = []

    const matchMatrix = _matchMatrix(scores)

    function addNoY([x, y]: Cell){
        scores[x][y] = 1
    }

    function addNoX([x, y]: Cell){
        scores[x] = []
        addNoY([x, y])
    }

    function addLive([x, y]: Cell){
        scores[x][y]++
    }

    const add = matchMatrix({
        noX: addNoX,
        noY: addNoY,
        live: addLive,
    })

    function accumulateNeighbours(neighbours: Array<Cell>){
        neighbours.forEach(add)
    }

    type ScoreGen = Generator<CellAndCount, void, void>

    function* yieldCounts(): ScoreGen {
        for(const i in scores){
            for(const j in scores[i]){
                const x = Number(i)
                const y = Number(j)
                yield [x, y, scores[x][y]]
            }
        }
    }

    return {
        accumulateNeighbours,
        yieldCounts,
    }
}
