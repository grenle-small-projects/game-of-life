import { Cell } from '../types/types.ts'
import { Board } from '../board/board.ts'
import { CountedNeighbours } from '../counted-neighbours/counted-neighbours.ts'
import neighbourhood from '../neighbourhood/neighbourhood.ts'

export function step(board: Board){
    const newBoard = Board()
    const countedNeighbours = CountedNeighbours()
    for(const cell of board.yieldCells()){
        const neighbours = neighbourhood(cell)
        countedNeighbours.accumulateNeighbours(neighbours)
    }
    for(const xyscore of countedNeighbours.yieldCounts()){
        const [ x, y, score ] = xyscore
        const newCell: Cell = [ x, y ]
        if(score === 3){
            newBoard.set(newCell)
        }
        if(score === 4){
            if(board.isAlive(newCell)){
                newBoard.set(newCell)
            }
        }
    }
    return newBoard
}

export function statefullStep(board: Board){
    const countedNeighbours = CountedNeighbours()
    for(const cell of board.yieldCells()){
        const neighbours = neighbourhood(cell)
        countedNeighbours.accumulateNeighbours(neighbours)
    }
    for(const xyscore of countedNeighbours.yieldCounts()){
        const [ x, y, score ] = xyscore
        const newCell: Cell = [ x, y ]
        if(score === 3){
            board.set(newCell)
        }
        else if(score === 4){
            if(board.isAlive(newCell)){
                board.set(newCell)
            }
        }
        else {
            board.unset(newCell)
        }
    }
    return board
}
