import { describe, expect, test } from 'bun:test'
import { Board } from '../board/board'
import { Cells } from '../types/types'
import { step } from './step'

describe('step', () => {

    test('step [] → []', () => {
        const board: Board = Board()
        const actual = Array.from(step(board).yieldCells())
        const expected = Array.from(Board().yieldCells())
        expect(actual).toStrictEqual(expected)
    })

    test('step with horizontal blinker', () => {
        const cells: Cells = [
            [ 0, 1 ], [ 1, 1 ], [ 2, 1 ],
        ]
        const board: Board = Board(cells)
        const actual = Array.from(step(board).yieldCells())
        const expected: Cells = [
            [ 1, 0 ], [ 1, 1 ], [ 1, 2 ],
        ]
        expect(actual).toStrictEqual(expected)
    })

    test('step with vertical blinker', () => {
        const cells: Cells = [
            [ 1, 0 ], [ 1, 1 ], [ 1, 2 ],
        ]
        const board: Board = Board(cells)
        const actual = Array.from(step(board).yieldCells())
        const expected: Cells = [
            [ 0, 1 ], [ 1, 1 ], [ 2, 1 ],
        ]
        expect(actual).toStrictEqual(expected)
    })
})
