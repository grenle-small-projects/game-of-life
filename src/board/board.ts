import { Cell, Cells } from '../types/types.ts'

export type Board = {
    set: (cell: Cell) => void
    unset: (cell: Cell) => void
    yieldCells: () => Generator<Cell, void, unknown>,
    isAlive: (cell: Cell) => boolean
}

export function Board(firstGeneration: Cells = []): Board {
    type Grid = Map<number, Set<number>>

    const grid: Grid = new Map()

    function set([x, y]: Cell){
        if(!grid.has(x)){
            grid.set(x, new Set())
            grid.get(x)?.add(y)
        }
        else{
            grid.get(x)?.add(y)
        }
    }

    function unset([x, y]: Cell){
        if(grid.has(x)){
            grid.get(x)?.delete(y)
        }
    }

    function* yieldCells(): Generator<Cell, void, unknown> {
        for(const [x, ys] of grid){
            for(const y of ys){
                yield [x, y]
            }
        }
    }

    function isAlive([x, y]: Cell){
        const column = grid.get(x)
        if(column === undefined){
            return false
        }
        else {
            return column.has(y)
        }
    }

    for(const [x, y] of firstGeneration){
        const column = grid.get(x)
        if(column === undefined){
            grid.set(x, new Set([y]))
        }
        else {
            column.add(y)
        }
    }

    return {
        set, unset,
        yieldCells,
        isAlive,
    }
}
