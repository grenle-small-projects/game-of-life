import { NumberToNumber } from '../types/types.ts'

export const succ: NumberToNumber = n => n + 1
export const pred: NumberToNumber = n => n - 1
export const i: <T>(x: T) => T = x => x

/**
 * [ f, g, ... z(x) ] → x → [ f(x), g(x), ..., z(x) ]
 * Array<T => U> → T → Array<U>
 */
export function pam<T, U>(fs: Array<(arg: T) => U>) {
    return function(arg: T): Array<U> {
        return fs.map(f => f(arg))
    }
}
