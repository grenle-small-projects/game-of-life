import { describe, expect, test } from 'bun:test'

import {
    i,
    pred,
    succ,
    pam,
} from './utils.ts'

describe('utils', () => {
    test('succ of 0 is 1', () => {
        expect(succ(0)).toStrictEqual(1)
    })
    
    test('pred of 1 is 0', () => {
        expect(pred(1)).toStrictEqual(0)
    })
    
    test('i of 9 is 9', () => {
        expect(i(9)).toStrictEqual(9)
    })
    
    test('i of \'hello\' is \'hello\'', () => {
        expect(i(9)).toStrictEqual(9)
    })
    
    test('pam [] 3 → []', () => {
        expect(pam([])(3)).toStrictEqual([])
    })
    
    test('pam [ succ ] 3 → [ 4 ]', () => {
        expect(pam([succ])(3)).toStrictEqual([4])
    })
    
    test('pam [succ, pred] 3 → [ 4, 2 ]', () => {
        expect(pam([succ, pred])(3)).toStrictEqual([4, 2])
    })

})
