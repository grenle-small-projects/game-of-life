import { Board } from '../board/board.ts'
import { step } from '../step/step.ts'
import { Viewport } from '../types/types.ts'

type DisplaySpeed = (n: number) => void

export function stepper(_viewport: Viewport, context: CanvasRenderingContext2D, board: Board, displaySpeed: DisplaySpeed){
    const t1 = performance.now()
    const newBoard = step(board)
    //const newBoard = statefullStep(board)
    requestAnimationFrame(_ => display(_viewport, context, newBoard, displaySpeed))
    const t2 = performance.now()
    displaySpeed(t2 - t1)
}

function display(viewPort: Viewport, context: CanvasRenderingContext2D, board: Board, displaySpeed: DisplaySpeed){
    const { viewX, viewY, cellSize } = viewPort
    const minX = viewX
    const maxX = viewX + (800 / cellSize)
    const minY = viewY
    const maxY = viewY + (800 / cellSize)
    context.fillStyle = "black"
    context.fillRect(0, 0, 800, 800)
    context.fillStyle = "grey"
    for(const colrow of board.yieldCells()){
        const [x, y] = colrow
        if(x > minX && x < maxX){
            if(y > minY && y < maxY){
                context.fillRect((x - minX) * cellSize, (y - minY) * cellSize, cellSize, cellSize)
            }
        }
    }
    stepper(viewPort, context, board, displaySpeed)
}
