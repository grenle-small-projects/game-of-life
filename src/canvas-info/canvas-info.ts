export type CanvasInfo = {
    element: HTMLCanvasElement,
    context: CanvasRenderingContext2D,
}

export function getCanvasInfo(): CanvasInfo | null {
    const canvas = document.getElementById('canvas') as HTMLCanvasElement
    if(canvas === null){
        console.error('element #canvas not found')
        return null
    }
    else if(canvas.nodeName !== 'CANVAS'){
        console.error('element #canvas is not a canvas')
        return null
    }
    else {
        const context = canvas.getContext("2d")
        if(context === null){
            console.error('could not obtain context form canvas')
            return null
        }
        else {
            return { element: canvas, context }
        }
    }
}
