import { PointMove, Cell } from '../types/types.ts'
import { succ, pred, i, pam } from '../utils/utils.ts'

export const northwest : PointMove = [ pred, pred ]
export const north     : PointMove = [ i,    pred ]
export const northeast : PointMove = [ succ, pred ]
export const west      : PointMove = [ pred, i    ]
export const center    : PointMove = [ i,    i    ]
export const east      : PointMove = [ succ, i    ]
export const southwest : PointMove = [ pred, succ ]
export const south     : PointMove = [ i,    succ ]
export const southeast : PointMove = [ succ, succ ]

export const neighboursGrid: Array<PointMove> = [
    northwest, north,     northeast,
    west,      center, east,
    southwest, south,     southeast,
]

export function movePoint([fx, fy]: PointMove) {
    return function([x, y]: Cell): Cell {
        return [ fx(x), fy(y) ]
    }
}

export const neighbourhood = pam(neighboursGrid.map(movePoint))

