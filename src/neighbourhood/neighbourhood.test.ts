import { describe, expect, test } from 'bun:test'

import {
    northeast,
    north,
    northwest,
    east,
    center,
    west,
    southeast,
    south,
    southwest,
    movePoint,
} from './_neighbourhood.ts'

describe('neighbourhood', () => {
    describe('directions', () => {
        test('northwest of [ 1, 1 ] is [ 0, 0 ]', () => {
            const move = movePoint(northwest)
            const actual = move([ 1, 1 ])
            expect(actual).toStrictEqual([ 0, 0 ])
        })
        test('north of [ 1, 1 ] is [ 1, 0 ]', () => {
            const move = movePoint(north)
            const actual = move([ 1, 1 ])
            expect(actual).toStrictEqual([ 1, 0 ])
        })
        test('northeast of [ 1, 1 ] is [ 0, 0 ]', () => {
            const move = movePoint(northeast)
            const actual = move([ 1, 1 ])
            expect(actual).toStrictEqual([ 2, 0 ])
        })
        test('west of [ 1, 1 ] is [ 0, 1 ]', () => {
            const move = movePoint(west)
            const actual = move([ 1, 1 ])
            expect(actual).toStrictEqual([ 0, 1 ])
        })
        test('samepoint of [ 1, 1 ] is [ 1, 1 ]', () => {
            const move = movePoint(center)
            const actual = move([ 1, 1 ])
            expect(actual).toStrictEqual([ 1, 1 ])
        })
        test('east of [ 1, 1 ] is [ 2, 1 ]', () => {
            const move = movePoint(east)
            const actual = move([ 1, 1 ])
            expect(actual).toStrictEqual([ 2, 1 ])
        })
        test('southwest of [ 1, 1 ] is [ 0, 2 ]', () => {
            const move = movePoint(southwest)
            const actual = move([ 1, 1 ])
            expect(actual).toStrictEqual([ 0, 2 ])
        })
        test('south of [ 1, 1 ] is [ 1, 2 ]', () => {
            const move = movePoint(south)
            const actual = move([ 1, 1 ])
            expect(actual).toStrictEqual([ 1, 2 ])
        })
        test('southeast of [ 1, 1 ] is [ 2, 2 ]', () => {
            const move = movePoint(southeast)
            const actual = move([ 1, 1 ])
            expect(actual).toStrictEqual([ 2, 2 ])
        })
    })
})

