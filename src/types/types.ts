
export type Cell  = [ number, number ]
export type Cells = Array<Cell>
export type CellAndCount = [ ...Cell, number ]

export type NumberToNumber = (arg: number) => number
export type PointMove = [ NumberToNumber, NumberToNumber ]

export type Viewport = {
    viewX: number,
    viewY: number,
    cellSize: number,
}
